package com.devraj.controller;

import com.devraj.dto.XMLDto;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

@Controller
public class ReportController {

    final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    DynamicReportGen dynamicReportGen;

    @PostMapping("report-gen")
    public ResponseEntity<InputStreamResource> reportGen(@RequestBody XMLDto xmlDto, HttpServletResponse response) throws FileNotFoundException {
            log.info("Preparing the pdf report via jasper.");
            try {


                JasperDesign jasperDesign = dynamicReportGen.getJasperDesign(xmlDto);
                JasperCompileManager.compileReportToFile(jasperDesign, "/home/to/reports/NoXmlDesignReport.jasper");

                File initialFile = new File("/home/to/reports/NoXmlDesignReport.jasper");
                InputStream targetStream = new FileInputStream(initialFile);
//
                dynamicReportGen.createJasperReport(targetStream);

                log.info("File successfully saved at the given path.");
            } catch (final Exception e) {
                log.error("Some error has occurred while preparing the employee pdf report.");
                e.printStackTrace();
            }

            // to display on a browser
            String fileName = "/home/to/John/xml_report.pdf";
            File file = new File(fileName);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.add("content-disposition", "inline;filename=" +file.getName());
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(file.length())
                    .contentType(MediaType.parseMediaType("application/pdf"))
                    .body(resource);
        }

}
