package com.devraj.controller;

import com.devraj.dto.XMLDto;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.*;
import net.sf.jasperreports.engine.type.*;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class DynamicReportGen {

    public static JasperDesign getJasperDesign(XMLDto xmlDto) throws JRException, JRException {
        //JasperDesign
        JasperDesign jasperDesign = new JasperDesign();
        jasperDesign.setName("NoXmlDesignReport");
        jasperDesign.setPageWidth(1100);
        jasperDesign.setPageHeight(900);
        jasperDesign.setColumnWidth(515);
        jasperDesign.setColumnSpacing(0);
        jasperDesign.setLeftMargin(40);
        jasperDesign.setRightMargin(40);
        jasperDesign.setTopMargin(50);
        jasperDesign.setBottomMargin(50);

        //Fonts
        JRDesignStyle normalStyle = new JRDesignStyle();
        normalStyle.setName("Sans_Normal");
        normalStyle.setDefault(true);
        normalStyle.setFontName("DejaVu Sans");
        normalStyle.setFontSize(12f);
        normalStyle.setPdfFontName("Helvetica");
        normalStyle.setPdfEncoding("Cp1252");
        normalStyle.setPdfEmbedded(false);
        jasperDesign.addStyle(normalStyle);

        JRDesignStyle boldStyle = new JRDesignStyle();
        boldStyle.setName("Sans_Bold");
        boldStyle.setFontName("DejaVu Sans");
        boldStyle.setFontSize(12f);
        boldStyle.setBold(true);
        boldStyle.setPdfFontName("Helvetica-Bold");
        boldStyle.setPdfEncoding("Cp1252");
        boldStyle.setPdfEmbedded(false);
        jasperDesign.addStyle(boldStyle);

        JRDesignStyle italicStyle = new JRDesignStyle();
        italicStyle.setName("Sans_Italic");
        italicStyle.setFontName("DejaVu Sans");
        italicStyle.setFontSize(12f);
        italicStyle.setItalic(true);
        italicStyle.setPdfFontName("Helvetica-Oblique");
        italicStyle.setPdfEncoding("Cp1252");
        italicStyle.setPdfEmbedded(false);
        jasperDesign.addStyle(italicStyle);

        //Parameters
        JRDesignParameter parameter = new JRDesignParameter();
        parameter.setName("ReportTitle");
        parameter.setValueClass(java.lang.String.class);
        jasperDesign.addParameter(parameter);

        //Query
        JRDesignQuery query = new JRDesignQuery();
//        query.setText("SELECT * FROM Address $P!{OrderByClause}");
        query.setText(xmlDto.getQuery());
        jasperDesign.setQuery(query);

        //Fields

        Map<String, Object> obs = xmlDto.getFields();
        Set<String> fields = obs.keySet();
        fields.forEach(fieldOb -> {

            try {
                JRDesignField field = new JRDesignField();
                field.setName(fieldOb);
                field.setValueClass(obs.get(fieldOb).getClass());
                jasperDesign.addField(field);
            } catch (JRException e) {
                e.printStackTrace();
            }
        });

        if (xmlDto.getExpressions() != null) {
            Map<String, String> operations = xmlDto.getExpressions();
            Set<String> keySet = operations.keySet();
            keySet.forEach(key -> {
                //Variables
                JRDesignVariable variable = new JRDesignVariable();
                variable.setName("total"+key);
                variable.setValueClass(java.lang.Double.class);
                variable.setResetType(ResetTypeEnum.REPORT);
                variable.setCalculation(operations.get(key).toUpperCase().equals("SUM") ? CalculationEnum.SUM : CalculationEnum.SYSTEM);

                JRDesignExpression expression = new JRDesignExpression();
                expression.setText("$F{"+key+"}");
                variable.setExpression(expression);
                try {
                    jasperDesign.addVariable(variable);
                } catch (JRException e) {
                    e.printStackTrace();
                }
            });
        }

        // band start

        //Page header
        JRDesignBand band = new JRDesignBand();
        band.setHeight(20);

        JRDesignFrame frame = new JRDesignFrame();
        frame.setX(0);
        frame.setY(5);
        frame.setWidth(900);
        frame.setHeight(15);
        frame.setForecolor(new Color(0x33, 0x33, 0x33));
        frame.setBackcolor(new Color(0x33, 0x33, 0x33));
        frame.setMode(ModeEnum.OPAQUE);
        band.addElement(frame);
        AtomicInteger counter = new AtomicInteger(0);
        fields.forEach(fieldOb -> {
            JRDesignStaticText staticText = new JRDesignStaticText();
            staticText.setX(counter.get());
            staticText.setY(0);
            staticText.setWidth(55);
            staticText.setHeight(15);
            staticText.setForecolor(Color.white);
            staticText.setBackcolor(new Color(0x33, 0x33, 0x33));
            staticText.setMode(ModeEnum.OPAQUE);
            staticText.setStyle(boldStyle);
            staticText.setText(fieldOb);
            frame.addElement(staticText);
            counter.getAndAdd(80);
        });
        jasperDesign.setPageHeader(band);

        //Column header
        band = new JRDesignBand();
        jasperDesign.setColumnHeader(band);

        //Detail
//        band = new JRDesignBand();
        final JRDesignBand band1 = new JRDesignBand();
        band1.setHeight(20);


        AtomicInteger counter1 = new AtomicInteger(0);
        fields.forEach(fieldOb -> {
                JRDesignTextField textField = new JRDesignTextField();
                JRDesignLine line = new JRDesignLine();

                textField = new JRDesignTextField();
                textField.setTextAdjust(TextAdjustEnum.SCALE_FONT);
                textField.setX(counter1.get());
                textField.setY(4);
                textField.setWidth(50);
                textField.setHeight(15);
                textField.setStyle(normalStyle);

                JRDesignExpression expression1 = new JRDesignExpression();

                expression1 = new JRDesignExpression();
                expression1.setValueClass(obs.get(fieldOb).getClass());
                expression1.setText("$F{"+fieldOb+"}");
                textField.setExpression(expression1);
                band1.addElement(textField);

                line = new JRDesignLine();
                line.setX(0);
                line.setY(19);
                line.setWidth(515);
                line.setHeight(0);
                line.setForecolor(new Color(0x80, 0x80, 0x80));
                line.setPositionType(PositionTypeEnum.FLOAT);
                band1.addElement(line);
                counter1.getAndAdd(80);
        });
        ((JRDesignSection)jasperDesign.getDetailSection()).addBand(band1);

        //Column footer
        band = new JRDesignBand();
        jasperDesign.setColumnFooter(band);

        //Page footer
        band = new JRDesignBand();
        jasperDesign.setPageFooter(band);

        //Summary
        final JRDesignBand bandSummary = new JRDesignBand();
        bandSummary.setHeight(25);
        bandSummary.setSplitType(SplitTypeEnum.STRETCH);

        AtomicInteger counter2 = new AtomicInteger(0);

        if (xmlDto.getExpressions() != null) {
            Map<String, String> operations = xmlDto.getExpressions();
            Set<String> keySet = operations.keySet();
            keySet.forEach(key -> {
                JRDesignTextField textField = new JRDesignTextField();
                textField = new JRDesignTextField();
                textField.setTextAdjust(TextAdjustEnum.STRETCH_HEIGHT);
                textField.setX(counter2.get());
                textField.setY(4);
                textField.setWidth(200);
                textField.setHeight(15);
                textField.setPositionType(PositionTypeEnum.FLOAT);
                textField.setStyle(normalStyle);

                JRDesignExpression expression = new JRDesignExpression();
                expression.setValueClass(java.lang.Double.class);
                expression.setText("$V{total"+key+"}");
                textField.setExpression(expression);
                bandSummary.addElement(textField);
                jasperDesign.setSummary(bandSummary);
                counter2.getAndAdd(80);
            });
        }

        return jasperDesign;
    }

    // Method to create the pdf file using the employee list datasource.
    public JasperPrint createJasperReport(InputStream file) throws JRException, IOException, ClassNotFoundException, SQLException {
        Connection connection;
        // Fetching the .jrxml file from the resources folder.

        // Compile the Jasper report from .jrxml to .japser
//        final JasperReport report = JasperCompileManager.compileReport(file);
        final JasperReport report = (JasperReport) JRLoader.loadObjectFromFile("/home/to/reports/NoXmlDesignReport.jasper");

        // Adding the additional parameters to the pdf.
        final Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy", "Dave");

        // Filling the report with the employee data and additional parameters information.
//        final JasperPrint print = JasperFillManager.fillReport(report, parameters, source);

        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jasper?user=root&password=secret");

        final JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);

        // Users can change as per their project requirements or can take it as request input requirement.
        // For simplicity, this tutorial will automatically place the file under the "c:" drive.
        // If users want to download the pdf file on the browser, then they need to use the "Content-Disposition" technique.
        final String filePath = "/home/to/John/";
        // Export the report to a PDF file.
        JasperExportManager.exportReportToPdfFile(print, filePath + "xml_report.pdf");
        return print;
    }
}
