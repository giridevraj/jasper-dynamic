package com.devraj;

import com.devraj.entity.Account;
import com.devraj.entity.Dealer;
import com.devraj.repository.AccountRepository;
import com.devraj.repository.DealerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class JaspernoxmlApplication {

    public static void main(String[] args) {
        SpringApplication.run(JaspernoxmlApplication.class, args);
    }

    @Bean
    CommandLineRunner init(DealerRepository dealerRepository, AccountRepository accountRepository) {

        return args -> {

            if (dealerRepository.findAll().size() == 0) {
                for (int i =0; i< 2000; i++) {
                    Dealer dealer = new Dealer();
                    dealer.setAddress("addr"+i);
                    dealer.setBalance((double)i * 10);
                    dealer.setEmail("email"+i+"@gmail.com");
                    dealer.setMobile("984249"+i);
                    dealer.setName("dealer "+i);
                    dealer.setNumber((double)i*3);
                    dealerRepository.save(dealer);
                }
            }

            if (accountRepository.findAll().size() == 0) {
                for (int i =0; i< 2000; i++) {
                    Account account = new Account();
                    account.setAccountNo(System.currentTimeMillis()+"A");
                    account.setBalance((double)i * Math.ceil(Math.random()/100));
                    accountRepository.save(account);
                }
            }

        };

    }
}
