# jasperreports-dynamic

This project is all about creating dynamic report using **java , jasper and JasperDesign**

# Project Setup

**create file for example _NoXmlDesignReport.jasper_**, 

**Open** *application.properties* file and change jdbc *username* and *password* or *port*

**Open** *DynamicReportGen.java* file and change jdbc *username* and *password* or *port*

**Run the project** , at first run 2000 account and 2000 dealers will be created automatically.

**Use _PostMan_ to post request** *localhost:8080/report-gen*

Post Request Examples:
********************************************
**Request with query, fields, and expression**

{
    "query": "select * from account a",
    "fields":  {
        "id": 1,
        "account_no": "dev",
        "balance": 300
        },
    "expressions": {
        "balance": "sum"
    }
}

***note: only _sum_ is allowed currently on expression, fields are the field present on the query that you want to show on report, and it must match to returned field from the query***
***********************************************
**Request with query and fields only, without expressions**

{
    "query": "select * from account a",
    "fields":  {
        "id": 1,
        "account_no": "dev",
        "balance": 300
        }
}

**************************************
**Request with different query, fields and expressions**

{
    "query": "select * from dealer d",
    "fields":  {
        "id": 1,
        "address": "dev",
        "balance": 300,
        "email": "ab",
        "mobile": "a",
        "number": 2.0
        },
    "expressions": {
        "balance": "sum",
        "number": "sum"
    }
}

****************************************
**Request with different query and fields only**

{
    "query": "select * from dealer d",
    "fields":  {
        "id": 1,
        "address": "dev",
        "balance": 300,
        "email": "ab",
        "mobile": "a",
        "number": 2.0
        }
}

*********************************************
**Request with different query,fields and expressions**
{
    "query": "select d.address,d.balance from dealer d",
    "fields":  {
        "address": "dev",
        "balance": 300
        },
    "expressions": {
        "balance": "sum"
    }
}


**note:: file will be saved on path on the disk when url is 
hit with either of the request example on the location _/home/to/xml_report.pdf_**
